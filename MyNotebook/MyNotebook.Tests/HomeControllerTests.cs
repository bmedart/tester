﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using MyNotebook.Controllers;
using MyNotebook.Models;

namespace MyNotebook.Tests
{
    
    [TestFixture]
    public class HomeControllerTests
    {
        
        [Test]
        public void Puts_Message_In_ViewBag()
        {
            var controller = new HomeController();
            var result = controller.Index();
           Assert.IsNotNull(result.ViewBag.Message);
        }

        [Test]
        public void About_Message_In_ViewBag()
        {
            var controller = new HomeController();
            var result = controller.About();
            Assert.IsNotNull(result.ViewBag.Message);
        }

        [Test]
        public void Contact_Message_In_ViewBag()
        {
            var controller = new HomeController();
            var result = controller.Contact();
            Assert.IsNotNull(result.ViewBag.Message);
        }

        // This is the test for the model Entry
        [Test]
        public void test_Entry()
        {
            Entry target = new Entry();
            const string TITLE = "Test Title";
            const string TEXT = "Hello World";
            const string TAG = "Penguin";
            DateTime Now = DateTime.Now;

            target.Title = TITLE;
            target.Text = TEXT;
            target.Date = Now;
            target.Tag = TAG;

            Assert.AreEqual(TITLE, target.Title);
            Assert.AreEqual(TEXT, target.Text);
            Assert.AreEqual(Now, target.Date);
            Assert.AreEqual(TAG, target.Tag);
        }

        
       
    }
}
