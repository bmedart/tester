﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyNotebook.Models;

namespace MyNotebook.Controllers
{
   
    public class HomeController : Controller
    {
        
        // GET: /Home/
        public MyNotebookEntity noteDB = new MyNotebookEntity();
        
        [AllowAnonymous]
        [HttpGet]
        public ViewResult Index()
        {

          
            noteDB.Database.CreateIfNotExists();

            return View(noteDB.Entries.ToList());
            
        }

        [Authorize(Users="Admin")]
        [HttpGet]
        public ViewResult newEntry()
        {
            
            return View();
        }

        
        [HttpPost]
        public ActionResult newEntry(Entry aEntry)
        {
            
            if (ModelState.IsValid)
            {
                noteDB.Entries.Add(aEntry);
                
                noteDB.SaveChanges();


                return RedirectToAction("Index");
            }
            return View(aEntry);
        }
         
        public ViewResult About()
        {
            ViewBag.Message = "Welcome To The About us Page!";
            return View();
        }

        public ViewResult Contact()
        {
            ViewBag.Message = "Welcome To The Contact page!";
            return View();
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult deleteDB(int id)
        {
            Entry entry = noteDB.Entries.Find(id);
            noteDB.Entries.Remove(entry);
            noteDB.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult SearchIndex(string searchString, string tagKeyword)
        {
            var tagList = new List<string>();
            var tagQuery = from d in noteDB.Entries
                           orderby d.Tag
                           select d.Tag;
            tagList.AddRange(tagQuery.Distinct());
            ViewBag.tagKeyword = new SelectList(tagList);


            var query = from m in noteDB.Entries
                        select m;

            if (!string.IsNullOrEmpty(searchString))
            {
                query = query.Where(s => s.Title.Contains(searchString));
            }
            if (string.IsNullOrEmpty(tagKeyword))
                return View(query);
            else
            {
                return View(query.Where(x => x.Tag == tagKeyword));
            }
            
        }
    }
}
