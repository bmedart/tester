﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MyNotebook.Models
{
    public class MyNotebookEntity : DbContext
    {

        
        public DbSet<Entry> Entries { get; set; }
    }
}