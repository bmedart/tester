﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace MyNotebook.Models
{
    public class Entry
    {
        [Required(ErrorMessage = "An Entry Title is Required")]
        public string Title { get; set; }

        [Required(ErrorMessage="Entry Text is Required")]
        public string Text { get; set; }

        [Required(ErrorMessage="Entry Date is Required")]
        public DateTime Date { get; set; }

        [Required(ErrorMessage="Entry Tag is Required")]
        public string Tag { get; set; }

        public int entryID { get; set; } // Primary Key
       
        

    }
    
}